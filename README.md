Source Jupyter Octave + pythons2 + python3 + R + bash notebook docker 
containers is found in

```
    ./src/octave-notebook
```

Note that in addition to the Jupyter notebook container we
also have submodules for nginx and docker-gen to provide an 
Nginx https proxy and auto-configation of the Nginx proxy 
(via docker-gen). These are here mainly to keep all the components 
for in one place and simplify provisioning the whole service onto 
arbitrary VMs.

So you probably don't need to look at or worry about anything except

```
    ./src/octave-notebook
```

